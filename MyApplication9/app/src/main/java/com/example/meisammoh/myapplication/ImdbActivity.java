package com.example.meisammoh.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.orhanobut.hawk.Hawk;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class ImdbActivity extends AppCompatActivity implements View.OnClickListener {
Context mContext=this;
 EditText word;
TextView title;
TextView director;
            ImageView poster;

            ProgressDialog dialog;

            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_imdb);


                bind();
                dialog = new ProgressDialog(mContext);
                dialog.setTitle("waiting");
                dialog.setCancelable(false);
                dialog.setMessage("Please wait for server response");


            }

            void bind() {
                word = (EditText) findViewById(R.id.word);
                title = (TextView) findViewById(R.id.title);
                director = (TextView) findViewById(R.id.director);
                poster = (ImageView) findViewById(R.id.poster);
                findViewById(R.id.search).setOnClickListener(this);

            }

            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.search) {
                    search(word.getText().toString());
                }
            }

            void search(String word) {
                   dialog.onStart();
                final String url = "http://www.omdbapi.com/?t=" + word + "&apikey=70ad462a";
                AsyncHttpClient client = new AsyncHttpClient();
                client.get(url, new TextHttpResponseHandler() {
                    @Override
                    public void onStart() {
                        super.onStart();
                        dialog.show();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Toast.makeText(mContext, "Error : "+ throwable, Toast.LENGTH_SHORT).show();



                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        // PublicMethods.showToast(mContext, responseString);
                        parseServerResponse(responseString);

                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        dialog.dismiss();
                    }
                });
            }


            void parseServerResponse(String serverResponse) {

                try {

                    JSONObject allObject = new JSONObject(serverResponse);

                    String titleValue = allObject.getString("Title");
                    String diretorValue = allObject.getString("Director");
                    String posterValue = allObject.getString("Poster");


                    title.setText(titleValue);
                    director.setText(diretorValue);
                    Picasso.get().load(posterValue).into(poster);

                } catch (Exception e) {

                    Toast.makeText(mContext, e.toString(), Toast.LENGTH_SHORT).show();

                }

            }


        }
